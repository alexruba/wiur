//
//  PointOfInterest.swift
//  Wiur
//
//  Created by Oxthor on 01/12/2016.
//  Copyright © 2016 Rubio. All rights reserved.
//

import Foundation
import MapKit
import SwiftyJSON
import Firebase

class Event: NSObject, MKAnnotation {
  enum EventStatus: Int {
    case approved
    case rejected
    case pending
  }
  
  enum DateRange: Int {
    case thisweek
    case upcoming
    case finished
  }
  
  var title: String?
  var uid: String = NSUUID().uuidString
  var address: String = ""
  var cp: String?
  var town: String?
  var category: String = ""
  var info: String = ""
  var price: Double = 0.0
  var mainImageURL: String = ""
  var extraImagesURL: [String] = []
  var coordinate: CLLocationCoordinate2D = CLLocationCoordinate2D()
  var start: Date = Date()
  var end: Date = Date()
  var user: String = ""
  var likes: Int = 0
  var ticketsURL: String?
  var website: String?
  var status: EventStatus = .pending
  var lastModified: Date = Date()
  var mainImage: UIImage?
  var extraImages: [UIImage] = []
  
  init(snapshot: DataSnapshot)
  {
    if let dict = snapshot.value as? NSDictionary {
      if let name = dict["name"] as? String {
        self.title = name
      }
      if let uid = dict["uid"] as? String {
        self.uid = uid
      }
      if let address = dict["address"] as? String {
        self.address = address
      }
      if let cp = dict["cp"] as? String {
        self.cp = cp
      }
      if let town = dict["town"] as? String {
        self.town = town
      }
      if let category = dict["category"] as? String {
        self.category = category
      }
      if let description = dict["description"] as? String {
        self.info = description
      }
      if let user = dict["user"] as? String {
        self.user = user
      }
      if let mainImageURL = dict["main_image"] as? String {
        self.mainImageURL = mainImageURL
      }
      if let ticketsURL = dict["tickets_url"] as? String {
        self.ticketsURL = ticketsURL
      }
      if let website = dict["website"] as? String {
        self.website = website
      }
      if let extraImagesData = dict["extra_images"] as? [String] {
        self.extraImagesURL = extraImagesData
      }
      if let price = dict["price"] as? Double {
        self.price = price
      }
      if let likeCount = dict["likeCount"] as? Int {
        self.likes = likeCount
      }
      if let status = dict["status"] as? Int {
        if let eventStatus = EventStatus(rawValue: status) {
          self.status = eventStatus
        }
      }
      if let location = dict["location"] as? [String: AnyObject] {
        if  let latitude = location["latitude"] as? Double,
            let longitude = location["longitude"] as? Double {
          self.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        }
      }
      
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = kDateFormatFirebase
      
      if  let startDate = dict["start_date"] as? String,
          let startFormatted = dateFormatter.date(from: startDate) {
        self.start = startFormatted
      }
      if  let endDate = dict["end_date"] as? String,
          let endFormatted = dateFormatter.date(from: endDate){
        self.end = endFormatted
      }
      if  let lastModifiedDate = dict["last_modified"] as? String,
          let lastFormatted = dateFormatter.date(from: lastModifiedDate){
        self.lastModified = lastFormatted
      }
    }
  }
  
  override init() {
    self.uid = NSUUID().uuidString
    self.title = ""
    self.address = ""
    self.cp = ""
    self.town = ""
    self.category = ""
    self.info = ""
    self.user = ""
    self.ticketsURL = ""
    self.website = ""
    self.price = 0.0
    self.start = Date()
    self.end = Date()
    self.extraImagesURL = [String]()
    self.mainImageURL = ""
    self.coordinate = CLLocationCoordinate2D()
    self.mainImage = UIImage()
    self.extraImages = [UIImage]()
    self.likes = 0
    self.status = .pending
    self.lastModified = Date()
  }
  
  func toDictionary() -> [String : Any] {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = kDateFormatFirebase
    
    let eventDictionary = [
      "address": self.address,
      "uid": self.uid,
      "category": self.category,
      "cp": self.cp ?? "",
      "town": self.town ?? "",
      "description": self.info,
      "user": self.user,
      "price": self.price,
      "website": self.website ?? "",
      "tickets_url": self.ticketsURL ?? "",
      "end_date": dateFormatter.string(from: self.end),
      "extra_images": self.extraImagesURL,
      "location": ["latitude": self.coordinate.latitude, "longitude": self.coordinate.longitude],
      "main_image": self.mainImageURL,
      "name": self.title!,
      "start_date": dateFormatter.string(from: self.start),
      "likes": self.likes,
      "status": self.status.hashValue,
      "last_modified": dateFormatter.string(from: self.lastModified)
      ] as [String : Any]
    
    return eventDictionary
  }
  
  func statusDescription() -> String {
    switch status
    {
    case .approved: return "Published".localized()
    case .rejected: return "Rejected".localized()
    case .pending: return "Pending review".localized()
    }
  }
  
  func statusImage() -> UIImage {
    switch status {
    case .approved: return #imageLiteral(resourceName: "green-circle")
    case .rejected: return #imageLiteral(resourceName: "red-circle")
    case .pending: return #imageLiteral(resourceName: "yellow-circle")
    }
  }
  
  func uploadMainImage(image: UIImage) -> StorageUploadTask {
    let mainImageuuid = NSUUID().uuidString + ".jpg"
    let mainImageStorageRef = Storage.storage().reference().child("events").child(uid).child(mainImageuuid)
    
    if let mainImageData = UIImageJPEGRepresentation(image, 0.3) {
      let uploadTask = mainImageStorageRef.putData(mainImageData)
      mainImageURL = mainImageStorageRef.fullPath
      return uploadTask
    }
    else {
      return StorageUploadTask()
    }
  }
  
  func uploadExtraImage(image: UIImage) -> StorageUploadTask {
    let extraImageUuid = NSUUID().uuidString + ".jpg"
    let extraImageStorageRef = Storage.storage().reference().child("events").child(uid).child(extraImageUuid)
    
    if let extraImageData = UIImageJPEGRepresentation(image, 0.5) {
      let uploadTask = extraImageStorageRef.putData(extraImageData)
      extraImagesURL.append(extraImageStorageRef.fullPath)
      return uploadTask
    }
    else {
      return StorageUploadTask()
    }
  }
  
  func range() -> DateRange {
    let today = Date()
    
    if today.isBetweeen(date: start.getWeekAgoDay(), andDate: start) ||
      today.isBetweeen(date: start, andDate: end) {
      return .thisweek
    } else if today.isEarlier(date: start.getWeekAgoDay()) {
      return .upcoming
    } else {
      return .finished
    }
  }
  
  func isCompleted() -> String {
    if let title = title, title.isEmpty {
      return "Name field is empty".localized()
    }
    
    if let website = website {
      if !website.isEmpty && UIApplication.shared.canOpenURL(URL(string: website)!) {
        return "Website URL is malformed, format requested is: http://www.mywebsite.com".localized()
      }
    }
    
    if let tickets = ticketsURL {
      if !tickets.isEmpty && UIApplication.shared.canOpenURL(URL(string: tickets)!) {
        return "Tickets page URL is not correct, format requested is: http://www.mywebsite.com".localized()
      }
    }
    
    if info.isEmpty {
      return "Description field is empty".localized()
    }
    
    if start == Date() {
      return "No starting date selected".localized()
    }
    
    if end == Date() {
      return "No ending date selected".localized()
    }
    
    if end.isEarlier(date: start) {
      return "Ending date must not be earlier than starting date".localized()
    }
    
    if category.isEmpty {
      return "No category selected".localized()
    }
    
    if coordinate.latitude == CLLocationCoordinate2D().latitude &&
       coordinate.longitude == CLLocationCoordinate2D().longitude {
      return "No location defined for the current event".localized()
    }
    
    return ""
  }
}

