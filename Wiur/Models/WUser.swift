//
//  WUser.swift
//  Wiur
//
//  Created by Oxthor on 20/03/2017.
//  Copyright © 2017 Rubio. All rights reserved.
//

import Foundation
import UIKit

class WUser: NSObject {
  var uid: String
  var name: String
  var username: String
  var email: String
  var type: UserType
  var avatarURL: String?
  var avatar: UIImage?
  
  static let sharedInstance = WUser()
  
  override init() {
    uid = ""
    name = ""
    username = ""
    email = ""
    type = .user
  }
  
  func updateWithData(data: Dictionary<String, Any>) {
    if let uid = data["uid"] as? String {
      self.uid = uid
    }
    if let name = data["name"] as? String {
      self.name = name
    }
    if let username = data["username"] as? String {
      self.username = username
    }
    if let email = data["email"] as? String {
      self.email = email
    }
    if let avatarURL = data["avatarURL"] as? String {
      self.avatarURL = avatarURL
    }
    if let type = data["type"] as? String {
      if let typeInt = Int(type) {
        if let userType = UserType(rawValue: typeInt) {
          self.type = userType
        }
      }
    }
  }
  
  func isUser() -> Bool {
    return type == .user
  }
  
  func isTeam() -> Bool {
    return type == .team
  }
}
