//
//  EventCollectionViewCell.swift
//  Wiur
//
//  Created by Oxthor on 26/02/2017.
//  Copyright © 2017 Rubio. All rights reserved.
//

import Foundation
import UIKit

class EventCollectionViewCell : UICollectionViewCell
{
  @IBOutlet weak var eventImage: UIImageView!
  @IBOutlet weak var categoryLabel: UILabel!
  @IBOutlet weak var imageActivityIndicatorView: UIActivityIndicatorView!
  
  @IBOutlet weak var eventContainerView: UIView!
  
  func configure(with event: Event)
  {
    eventImage.image = event.mainImage
    categoryLabel.text = event.category
    
    eventImage.layer.cornerRadius = 5
    eventContainerView.layer.cornerRadius = 5
    eventContainerView.layer.shadowColor = UIColor.black.cgColor
    eventContainerView.layer.shadowOffset = CGSize(width: 0, height: 0.5)
    eventContainerView.layer.shadowOpacity = 0.5
    eventContainerView.layer.shadowRadius = 3
  }
}
