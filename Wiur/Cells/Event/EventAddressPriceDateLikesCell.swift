//
//  EventAddressPriceDateLikesCell.swift
//  Wiur
//
//  Created by Oxthor on 20/02/2017.
//  Copyright © 2017 Rubio. All rights reserved.
//

import UIKit

class EventAddressPriceDateLikesCell : UITableViewCell
{
  @IBOutlet weak var eventImage: UIImageView!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var streetLabel: UILabel!
  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var priceLabel: UILabel!
  @IBOutlet weak var likesLabel: UILabel!
  @IBOutlet weak var imageActivityIndicatorView: UIActivityIndicatorView!
  
  func configure(with event: Event)
  {
    titleLabel.text = event.title
    dateLabel.text = event.start.dateOutput(format: kDefaultDateFormat)
    streetLabel.text = event.address
    likesLabel.text = event.likes.description
    eventImage.image = event.mainImage
    eventImage.rounded(cornerRadius: 5.0, borderWidth: 0.0, borderColor: .clear)
    
    if event.price == 0.0 { priceLabel.text = "Gratis".localized() }
    else                  { priceLabel.text = event.price.description + NSLocale.current.currencySymbol! }
    
  }
}
