//
//  EventFormViewController.swift
//  Wiur
//
//  Created by Oxthor on 23/02/2017.
//  Copyright © 2017 Rubio. All rights reserved.
//

import Foundation
import UIKit
import DatePickerDialog
import MapKit

protocol EventFormDelegate: class {
  func didFillName(name: String)
  func didFillDescription(eventDescription: String)
  func didFillCategory(category: String)
  func didFillWebpage(webpage: String)
  func didFillTicketsURL(ticketsURL: String)
  func didFillStartDate(startDate: Date)
  func didFillEndDate(endDate: Date)
  func didFillPrice(price: Double)
  func didFillLocation(location: CLPlacemark)
}

class EventFormViewController: UITableViewController, UITextFieldDelegate, SetMapPositionDelegate, CategoryTableViewDelegate, UITextViewDelegate {
  weak var activeField: UITextField?
  weak var delegate: EventFormDelegate?
  
  private var firstLoad: Bool = true
  
  var event: Event?
  
  // Event variables
  var location: CLPlacemark? = nil {
    didSet {
      self.delegate?.didFillLocation(location: location!)
    }
  }
  
  var category: String = "" {
    didSet {
      self.delegate?.didFillCategory(category: category)
    }
  }
  
  var name: String = "" {
    didSet {
      self.delegate?.didFillName(name: name)
    }
  }
  
  var eventDescription: String = "" {
    didSet {
      self.delegate?.didFillDescription(eventDescription: eventDescription)
    }
  }
  
  var webpage: String = "" {
    didSet {
      self.delegate?.didFillWebpage(webpage: webpage)
    }
  }
  
  var ticketsURL: String = "" {
    didSet {
      self.delegate?.didFillTicketsURL(ticketsURL: ticketsURL)
    }
  }
  
  var startDate: Date = Date() {
    didSet {
      self.delegate?.didFillStartDate(startDate: startDate)
    }
  }
  
  var endDate: Date = Date() {
    didSet {
      self.delegate?.didFillEndDate(endDate: endDate)
    }
  }
  
  var price: Double = 0.0 {
    didSet {
      self.delegate?.didFillPrice(price: price)
    }
  }
  
  override func viewDidLoad() {
    self.hideKeyboardWhenTappedAround()
    
    NotificationCenter.default.addObserver(self, selector: #selector(EventFormViewController.keyboardDidShow(notification:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(EventFormViewController.keyboardWillBeHidden(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    
  }
  
  override func viewDidAppear(_ animated: Bool) {
    // Fill table if it is editing mode
    if (event != nil && firstLoad) { fillTableViewWithEventInfo(); firstLoad = false }
  }
  
  func fillTableViewWithEventInfo() {
    for currentCell in tableView.visibleCells {
      let indexPath = tableView.indexPath(for: currentCell)
      
      if let event = event, let indexPath = indexPath {
        if let currentTextField = currentCell.viewWithTag(2) as? UITextField {
          if (indexPath.row == 0) { currentTextField.text = event.title }
          if (indexPath.row == 2) { currentTextField.text = event.category }
          if (indexPath.row == 3) { currentTextField.text = event.start.dateOutput(format: kDefaultDateFormat) }
          if (indexPath.row == 4) { currentTextField.text = event.end.dateOutput(format: kDefaultDateFormat) }
          if (indexPath.row == 5) { currentTextField.text = event.address }
          if (indexPath.row == 6) { currentTextField.text = event.price.description }
          if (indexPath.row == 7) { currentTextField.text = event.website }
          if (indexPath.row == 8) { currentTextField.text = event.ticketsURL }
        }
        else if let currentTextView = currentCell.viewWithTag(2) as? UITextView {
          if (indexPath.row == 1) { currentTextView.text = event.info }
        }
      }
    }
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let currentCell = tableView.cellForRow(at: indexPath)
    
    switch (indexPath.row)
    {
      case 0, 7, 8: // Nombre, página web y página web de entradas
        let currentCellTextField = currentCell?.viewWithTag(2) as! UITextField
        currentCellTextField.isUserInteractionEnabled = true
        currentCellTextField.becomeFirstResponder()
        break
      case 1: // Descripción
        let currentCellTextView = currentCell?.viewWithTag(2) as! UITextView
        currentCellTextView.isUserInteractionEnabled = true
        currentCellTextView.becomeFirstResponder()
        break
      case 2: // Categoría
        let nav = UIStoryboard.init(name: "CategoryTable", bundle: nil).instantiateInitialViewController() as! UINavigationController
        let vc = nav.viewControllers.first as! CategoryTableViewController
        vc.delegate = self
        self.present(nav, animated: true, completion: nil)
        break
      case 3, 4: // Fecha inicio y fin
        dateTextFieldTapped(currentCell!)
        break
      case 5: // Lugar
        let currentCellTextField = currentCell?.viewWithTag(2) as! UITextField
        locationTextFieldTapped(currentCellTextField)
        break
      case 6: // Precio
        let currentCellTextField = currentCell?.viewWithTag(2) as! UITextField
        currentCellTextField.isUserInteractionEnabled = true
        currentCellTextField.keyboardType = .decimalPad
        currentCellTextField.becomeFirstResponder()
        break
      default:
        break
    }
  }
  
  // MARK: Actions
  @IBAction func nameFieldDidChange(_ sender: UITextField) {
    if let nameText = sender.text {
      self.name = nameText
    }
  }
  
  @IBAction func priceFieldDidChange(_ sender: UITextField) {
    if let priceText = sender.text, let doublePrice = Double(priceText){
      self.price = doublePrice
    }
  }
  
  @IBAction func websiteFieldDidChange(_ sender: UITextField) {
    if let websiteText = sender.text {
      self.webpage = websiteText
    }
  }
  
  @IBAction func ticketsURLDidChange(_ sender: UITextField) {
    if let ticketsText = sender.text {
      self.ticketsURL = ticketsText
    }
  }
  
  func dateTextFieldTapped(_ sender: UITableViewCell) {
    let currentCellValueTextField = (sender.viewWithTag(2) as! UITextField)
    let startOrEndDate = tableView.indexPath(for: sender)?.row
    
    DatePickerDialog().show("Select a date".localized(), doneButtonTitle: "Done".localized(), cancelButtonTitle: "Cancel".localized(), datePickerMode: .dateAndTime) {
      (date) -> Void in
      if let date = date {
        if startOrEndDate == 3 { // Start date
          self.startDate = date
        } else if startOrEndDate == 4 { // End date 
          self.endDate = date
        }
        currentCellValueTextField.text = "\(date.description(with: Locale.current))".components(separatedBy: "(")[0]
        currentCellValueTextField.isUserInteractionEnabled = false
      }
    }
  }
  
  func locationTextFieldTapped(_ sender: UITextField) {
    let vc = UIStoryboard.init(name: "SetMapPosition", bundle: nil).instantiateInitialViewController() as! SetMapPositionViewController
    vc.setMapPositionDelegate = self

    self.navigationController?.pushViewController(vc, animated: true)
  }

  // MARK: UITextField Delegate
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    self.view.endEditing(true)
    return false
  }
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    self.activeField = nil
    textField.isUserInteractionEnabled = false
  }
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
    self.activeField = textField
  }
  
  // MARK: UITextView Delegate
  func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
    self.eventDescription = textView.text + text
    return true
  }
  
  func textViewDidEndEditing(_ textView: UITextView) {
    textView.isUserInteractionEnabled = false
  }
  
  // MARK: SetMapPosition Delegate
  func didSelectMapPosition(position: CLPlacemark) {
    self.location = position
    
    let cell = tableView.cellForRow(at: IndexPath(row: 5, section: 0))
    let cellLabel = cell?.viewWithTag(2) as! UITextField
    cellLabel.text = location?.addressDictionary?["Thoroughfare"] as! String?
  }
  
  // MARK: CategoryTableView Delegate
  func didSelectCategory(category: String) {
    self.category = category
    
    let cell = tableView.cellForRow(at: IndexPath(row: 2, section: 0))
    let cellLabel = cell?.viewWithTag(2) as! UITextField
    cellLabel.text = category
  }
  
  // MARK: Event information
  
  func keyboardDidShow(notification: NSNotification) {
    if let activeField = self.activeField, let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
      let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height, right: 0.0)
      tableView.contentInset = contentInsets
      tableView.scrollIndicatorInsets = contentInsets
      var aRect = self.view.frame
      aRect.size.height -= keyboardSize.size.height
      if (!aRect.contains(activeField.frame.origin)) {
        tableView.scrollRectToVisible(activeField.frame, animated: true)
      }
    }
  }
  
  func keyboardWillBeHidden(notification: NSNotification) {
    let contentInsets = UIEdgeInsets.zero
    tableView.contentInset = contentInsets
    tableView.scrollIndicatorInsets = contentInsets
  }
}
