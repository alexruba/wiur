//
//  PublishEventViewController.swift
//  Wiur
//
//  Created by Oxthor on 16/01/2017.
//  Copyright © 2017 Rubio. All rights reserved.
//

import Foundation
import Firebase
import UIKit
import DatePickerDialog
import MapKit

class PublishEventViewController : UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, EventFormDelegate
{
  @IBOutlet var extraImageButtons: [UIButton]!
  @IBOutlet weak var mainImageButton: UIButton!
  @IBOutlet weak var formContainerView: UIView!
  
  var currentImageButton = UIButton()
  var event: Event = Event()
  var imagesProcessed = 0
  var imagesCount = 0
  var user: User!
  
  override func viewDidLoad() {
    user = Auth.auth().currentUser
  }
  
  override func viewDidAppear(_ animated: Bool) {
    setDefaultNavigationController()
    checkCurrentUser()
  }
  
  func checkCurrentUser() {
    if WUser.sharedInstance.type == UserType.user {
      let alert = UIAlertController(title: "Error", message: "Get an organizer account in order to access this section".localized(), preferredStyle: .alert)
      let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
        self.tabBarController?.selectedIndex = 0
      })
      
      alert.addAction(okAction)
      self.present(alert, animated: true, completion: nil)
    }
  }
  
  // MARK: Publish Event
  
  @IBAction func publishEvent(_ sender: UIBarButtonItem) {
    let errors = event.isCompleted()
    if !errors.isEmpty {
      self.presentDefaultAlert(title: "Error", message: errors)
      return;
    }
    
    if isMainImageSet() {
      imagesCount = getExtraImagesCount() + 1
      
      let mainImageUploadTask = event.uploadMainImage(image: mainImageButton.currentImage!)
      
      mainImageUploadTask.observe(.success) { snapshot in
        self.imageProcessed()
      }
      
      for extraImageButton in self.extraImageButtons {
        if isExtraImageSet(extraButton: extraImageButton) {
          let extraImageUploadTask = event.uploadExtraImage(image: extraImageButton.currentImage!)
          
          extraImageUploadTask.observe(.success) { snapshot in
            self.imageProcessed()
          }
        }
      }
    }
    else {
      self.presentDefaultAlert(title: "Error", message: "No image selected for the current event".localized())
    }
  }
  
  func imageProcessed() {
    imagesProcessed += 1
    if imagesProcessed >= imagesCount {
      addNewEvent()
    }
  }
  
  func addNewEvent() {
    uploadEventData()
    showSuccessMessage()
  }
  
  func uploadEventData() {
    event.user = (Auth.auth().currentUser?.uid)!
    event.status = Event.EventStatus.pending
    
    let eventDictionary = event.toDictionary()
    DataService.dataService.EVENT_REF.child(event.uid).setValue(eventDictionary)
  }
  
  func showSuccessMessage() {
    let alertController = UIAlertController(title: "Event created".localized(), message: "Your event has been created successfully! Take a look at all its details in the Events or My Account tab".localized(), preferredStyle: .alert)
    
    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: { _ in
      self.tabBarController?.selectedIndex = 0
    })
    
    alertController.addAction(defaultAction)
    
    self.present(alertController, animated: true, completion: nil)
  }
  
  // MARK: Event photo
  @IBAction func didTapImage(_ sender: UIButton) {
    let picker = UIImagePickerController()
    picker.delegate = self
    currentImageButton = sender
    
    present(picker, animated: true, completion: nil)
  }
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    var selectedImageFromPicker: UIImage?
    
    if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
      selectedImageFromPicker = originalImage
    }
    
    if let selectedImage = selectedImageFromPicker {
      self.currentImageButton.setImage(selectedImage, for: .normal)
    }
    
    picker.dismiss(animated: true, completion: nil)
  }
  
  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    picker.dismiss(animated: true, completion: nil)
  }
  
  // MARK: Upload images utilities
  func isMainImageSet() -> Bool {
    return mainImageButton.currentImage != kMainImagePlaceholder
  }
  
  func isExtraImageSet(extraButton: UIButton) -> Bool {
    return extraButton.currentImage! != kExtraImagePlaceholder
  }
  
  func getExtraImagesCount() -> Int {
    var count = 0
    
    for extraImagesButton in extraImageButtons {
      if isExtraImageSet(extraButton: extraImagesButton) {
        count += 1
      }
    }
    
    return count
  }
  
  // MARK: Event Form delegate
  func didFillName(name: String) {
    self.event.title = name
  }
  
  func didFillPrice(price: Double) {
    self.event.price = price
  }
  
  func didFillStartDate(startDate: Date) {
    self.event.start = startDate
  }
  
  func didFillEndDate(endDate: Date) {
    self.event.end = endDate
  }
  
  func didFillWebpage(webpage: String) {
    self.event.website = webpage
  }
  
  func didFillCategory(category: String) {
    self.event.category = category
  }
  
  func didFillTicketsURL(ticketsURL: String) {
    self.event.ticketsURL = ticketsURL
  }
  
  func didFillLocation(location: CLPlacemark) {
    if let address = location.addressDictionary?["Thoroughfare"] {
      self.event.address = address as? String ?? ""
    }
    
    if let cp = location.addressDictionary?["ZIP"] {
      self.event.cp = cp as? String ?? ""
    }
    
    if let town = location.addressDictionary?["City"] {
      self.event.town = town as? String ?? ""
    }
    
    self.event.coordinate = (location.location?.coordinate)!
  }
  
  func didFillDescription(eventDescription: String) {
    self.event.info = eventDescription
  }
  
  // MARK: Prepare for segue
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "embed-event-form" {
      let vc = segue.destination as! EventFormViewController
      vc.delegate = self
    }
  }
}
