//
//  AccountOptionsTableViewController.swift
//  Wiur
//
//  Created by Oxthor on 27/02/2017.
//  Copyright © 2017 Rubio. All rights reserved.
//

import Foundation
import UIKit

protocol AccountOptionsDelegate: class {
  func didSelectMyEvents()
  func didSelectMyFavorites()
  func didSelectEditAccount()
  func didSelectImproveAccount()
  func didSelectSignOut()
}

class AccountOptionsTableViewController: UITableViewController {
  
  weak var delegate: AccountOptionsDelegate?
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    switch(indexPath.row) {
    case 0: self.delegate?.didSelectMyEvents()
      break
    case 1: self.delegate?.didSelectMyFavorites()
      break
    case 2: self.delegate?.didSelectEditAccount()
      break
    case 3: self.delegate?.didSelectImproveAccount()
      break
    case 4: self.delegate?.didSelectSignOut()
      break
    default: break
    }
    return
  }
  
}
