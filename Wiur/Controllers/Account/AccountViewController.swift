//
//  AccountViewController.swift
//  Wiur
//
//  Created by Oxthor on 15/01/2017.
//  Copyright © 2017 Rubio. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import Agrume

class AccountViewController : UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, AccountOptionsDelegate {
  
  @IBOutlet weak var usernameLabel: UILabel!
  @IBOutlet weak var userAvatar: UIImageView!
  @IBOutlet weak var backgroundImageView: UIImageView!
  @IBOutlet weak var settingsContainerView: UIView!

  override func viewDidLoad() {
    self.usernameLabel.text = WUser.sharedInstance.username
    
    setAvatarImage()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    setDefaultNavigationController()
  }
  
  // MARK: Account Options Delegate
  func didSelectSignOut() {
    let alertController = UIAlertController(title: "Exit".localized(), message: "Do you want to log out?".localized(), preferredStyle: .alert)
    
    let signOutAction = UIAlertAction(title: "OK", style: .default, handler: {(alert: UIAlertAction!) in
      try! Auth.auth().signOut()
      
      let loginStoryboard = UIStoryboard(name: "Login", bundle: nil)
      let vc = loginStoryboard.instantiateInitialViewController()
      
      let appDomain = Bundle.main.bundleIdentifier!
      UserDefaults.standard.removePersistentDomain(forName: appDomain)
      
      self.present(vc!, animated: true, completion: nil)
    })
    
    let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .default, handler: nil)
    
    alertController.addAction(signOutAction)
    alertController.addAction(cancelAction)
    
    self.present(alertController, animated: true, completion: nil)
  }
  
  func didSelectMyEvents() {
    self.performSegue(withIdentifier: kShowUserEvents, sender: nil)
  }
  
  func didSelectImproveAccount() {
//    self.presentDefaultAlert(title: NSLocalizedString("Próximamente", comment: ""), message: NSLocalizedString("Característica no disponible en estos momentos", comment: ""))
  }
  
  func didSelectEditAccount() {
    print("Edit account")
  }
  
  func didSelectMyFavorites() {
    self.performSegue(withIdentifier: kShowUserFavorites, sender: nil)
  }
  
  // MARK: Avatar Image
  @IBAction func handleProfileImage() {
    let picker = UIImagePickerController()
    picker.delegate = self
    picker.allowsEditing = true
    
    present(picker, animated: true, completion: nil)
  }
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    var selectedImageFromPicker: UIImage?
    
    if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
      selectedImageFromPicker = editedImage
    }
    else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
      selectedImageFromPicker = originalImage
    }
    
    if let selectedImage = selectedImageFromPicker
    {
      self.userAvatar.image = selectedImage
      
      var storageURL = ""
      if let avatarURL = WUser.sharedInstance.avatarURL {
        storageURL = avatarURL
      } else {
        let uuid = NSUUID().uuidString + ".jpg"
        storageURL = "user-avatars/" + uuid
      }
      
      let storageRef = Storage.storage().reference().child(storageURL)
      
      if let uploadData = UIImageJPEGRepresentation(selectedImage, 0.1) {
        storageRef.putData(uploadData, metadata: nil, completion: { (metadata, error) in
          if error != nil {
            print(error!)
            return
          }
          DataService.dataService.USER_REF.child(WUser.sharedInstance.uid).child("avatarURL").setValue(storageRef.fullPath)
        })
      }
    }
    
    picker.dismiss(animated: true, completion: nil)
    
  }
  
  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    picker.dismiss(animated: true, completion: nil)
  }
  
  func setAvatarImage() {
    self.userAvatar.rounded(cornerRadius: 50.0, borderWidth: 2.0, borderColor: UIColor.white)
    
    let activityIndicator = self.view.viewWithTag(5) as! UIActivityIndicatorView
    
    if WUser.sharedInstance.avatarURL != nil {
      DataService.dataService.downloadAvatar(user: WUser.sharedInstance, with: { 
        self.userAvatar.image = WUser.sharedInstance.avatar
        activityIndicator.stopAnimating()
      })
    }
    else
    {
      self.userAvatar.image = kUserPlaceholderImage
      activityIndicator.stopAnimating()
    }
  }
  
  // MARK: Prepare for segue
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == kEmbedAccountOptions {
      let vc = segue.destination as! AccountOptionsTableViewController
      vc.delegate = self
    }
    
    if segue.identifier == kShowUserEvents {
      let vc = segue.destination as! UserEventsTableViewController
      vc.query = DataService.dataService.EVENT_REF.queryOrdered(byChild: "user").queryEqual(toValue: WUser.sharedInstance.uid)
      vc.reuseCell = kCellEventAddressPriceDateLikes
      vc.showingMyEvents = true
    }
    
    if segue.identifier == kShowUserFavorites {
      let vc = segue.destination as! UserEventsTableViewController
      vc.query = DataService.dataService.LIKES_REF.child(WUser.sharedInstance.uid)
      vc.reuseCell = kCellEventAddressPriceDateLikes
    }
  }
}
