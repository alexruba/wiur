//
//  EditAccountController.swift
//  Wiur
//
//  Created by Oxthor on 13/03/2017.
//  Copyright © 2017 Rubio. All rights reserved.
//

import UIKit
import Firebase

class EditAccountController: UIViewController
{
  @IBOutlet weak var nameField: UITextField!
  @IBOutlet weak var usernameField: UITextField!
  
  var user: WUser = WUser.sharedInstance
  
  override func viewDidLoad() {
    setupUserInfo()
  }
  
  func setupUserInfo() {
    DataService.dataService.USER_REF.child(user.uid).observeSingleEvent(of: .value, with: {
      snapshot in
      let value = snapshot.value as? NSDictionary
      
      let username = value?["username"] as? String ?? NSLocalizedString("Unknown", comment: "")
      self.usernameField.text = username
      
      let name = value?["name"] as? String ?? NSLocalizedString("Unknown", comment: "")
      self.nameField.text = name
    })
  }
  
  
  // MARK: Actions
  @IBAction func updateUserInfo(_ sender: UIBarButtonItem) {
    if let name = nameField.text,
       let username = usernameField.text {
      if nameField.hasText && usernameField.hasText {
        let userDictionary = ["name": name, "username": username]
        DataService.dataService.USER_REF.child(user.uid).updateChildValues(userDictionary)
        
        dismiss(animated: true, completion: nil)
      } else {
        presentDefaultAlert(title: "Error", message: "Some data is missing".localized())
      }
    }
    
  }
  @IBAction func resetPassword(_ sender: Any) {
    Auth.auth().sendPasswordReset(withEmail: user.email)
    self.presentDefaultAlert(title: "Mail sent".localized(), message: "An email has been sent with information about how to change your password".localized())
  }
}
