//
//  UserEventsTableViewController.swift
//  Wiur
//
//  Created by Oxthor on 27/02/2017.
//  Copyright © 2017 Rubio. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class UserEventsTableViewController: UITableViewController
{
  var events = [Event]()
  var query = DatabaseQuery()
  var reuseCell = ""
  var showingMyEvents: Bool = false
  
  override func viewDidLoad() {
    query.removeAllObservers()
    tableView.register(UINib(nibName: reuseCell, bundle: nil), forCellReuseIdentifier: "EventCell")
    loadEventsWithQuery(query: query)
  }
  
  // MARK: Table View Data Source
  
  override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    if editingStyle == UITableViewCellEditingStyle.delete {
      DataService.dataService.EVENT_REF.child(events[indexPath.row].uid).removeValue()
      events.remove(at: indexPath.row)
      tableView.deleteRows(at: [indexPath as IndexPath], with: UITableViewRowAnimation.automatic)
    }
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "EventCell", for: indexPath) as! EventAddressPriceDateLikesCell
    cell.configure(with: events[indexPath.row])
    return cell
  }
  
  // MARK: Table View Delegate
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    self.performSegue(withIdentifier: kEventDetailSegue, sender: nil)
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return events.count
  }
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  // MARK: Utility methods
  func indexFor(event: Event) -> IndexPath {
    for row in 0..<events.count {
      if event.uid == events[row].uid {
        return IndexPath(row: row, section: 0)
      }
    }
    
    return IndexPath()
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if let viewController = segue.destination as? UINavigationController {
      let indexPath = self.tableView?.indexPathForSelectedRow
      let event = events[(indexPath?.row)!]
      let vc = viewController.viewControllers.first as! DetailViewController
      vc.currentEvent = event
      if(self.showingMyEvents) { vc.isEditable = true }
    }
  }
  
  // MARK: Firebase Database Load
  func loadEventsWithQuery(query: DatabaseQuery) {
    query.observe(.value, with: { (snapshot) in
      
      self.events.removeAll()
      
      if (snapshot.value as? [String:AnyObject]) != nil {
        for child in snapshot.children {
          let event = Event(snapshot: child as! DataSnapshot)
          self.events.append(event)
          self.tableView.reloadData()
          
          Storage.storage().reference().child(event.mainImageURL).getData(maxSize: 25*256*256, completion: { (data, error) in
            if error != nil {
              print(error!)
              return
            }
            DispatchQueue.main.async {
              let image = UIImage(data: data!)
              event.mainImage = image!
              let eventIndexPath = self.indexFor(event: event)
              if let cell = self.tableView?.cellForRow(at: eventIndexPath) as? EventAddressPriceDateLikesCell {
                cell.eventImage.image = event.mainImage
                cell.imageActivityIndicatorView.stopAnimating()
              }
            }
          })

        }
      } else {
        self.presentDefaultAlert(title: "Error".localized(), message: "No events found".localized())
      }
    })
  }
}
