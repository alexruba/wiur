//
//  DetailViewController.swift
//  Wiur
//
//  Created by Oxthor on 04/12/2016.
//  Copyright © 2016 Rubio. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import ImageSlideshow
import Agrume
import Firebase

class DetailViewController : UIViewController, UIScrollViewDelegate
{
  @IBOutlet weak var pageControl: UIPageControl!
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var containerView: UIView!
  @IBOutlet weak var infoScrollView: UIScrollView!
  @IBOutlet weak var infoContainerView: UIView!
  @IBOutlet weak var infoView: UIView!
  
  @IBOutlet weak var nameText: UILabel!
  @IBOutlet weak var descriptionText: UILabel!
  @IBOutlet weak var addressText: UILabel!
  @IBOutlet weak var startDateText: UILabel!
  @IBOutlet weak var endDateText: UILabel!
  @IBOutlet weak var websiteButton: UIButton!
  @IBOutlet weak var ticketsButton: UIButton!
  @IBOutlet weak var priceText: UILabel!
  @IBOutlet weak var usernameText: UILabel!
  @IBOutlet weak var userImage: UIImageView!
  @IBOutlet weak var lastModifiedText: UILabel!
  @IBOutlet weak var likesText: UILabel!
  
  @IBOutlet weak var likeButton: UIBarButtonItem!
  @IBOutlet weak var editButton: UIBarButtonItem!
  @IBOutlet weak var shareButton: UIBarButtonItem!
  
  var currentEvent = Event()
  var extra_images = [ImageSource]()
  var isEditable: Bool = false
  
  override func viewDidLoad() {
    super.viewDidLoad()
    scrollView.delegate = self

    getEventUser()
    setInfoTextForEvent()
    downloadExtraImages()
    checkEventLike()
  }
  
  override func viewDidLayoutSubviews() {
    let infoScrollViewHeight = infoView.bounds.height + descriptionText.bounds.height + nameText.bounds.height + usernameText.bounds.height + userImage.bounds.height + 200
    scrollView.contentSize = CGSize(width: containerView.bounds.width, height: scrollView.bounds.height)
    infoScrollView.contentSize = CGSize(width: infoContainerView.bounds.width, height: infoScrollViewHeight)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    self.extendedLayoutIncludesOpaqueBars = true
    userImage.rounded(cornerRadius: 10.0, borderWidth: 2.0, borderColor: UIColor.white)
    setNavigationBarItems()
  }
  
  func setNavigationBarItems() {
    if !isEditable {
      editButton.tintColor = UIColor.clear
      editButton.isEnabled = false
    }
    else {
      editButton.tintColor = UIColor.black
      editButton.isEnabled = true
    }
  }
  
  func getEventUser() {
    DataService.dataService.getUser(uid: currentEvent.user) { (user) in
      self.usernameText.text = user.username
      
      DataService.dataService.downloadAvatar(user: user) {
        self.userImage.image = user.avatar
      }
    }
  }
  
  func checkEventLike() {
    DataService.dataService.LIKES_REF.child(WUser.sharedInstance.uid).observeSingleEvent(of: .value, with: {
      snapshot in
      
      if snapshot.hasChild(self.currentEvent.uid) {
        self.likeButton.image = #imageLiteral(resourceName: "liked")
      } else {
        self.likeButton.image = #imageLiteral(resourceName: "like")
      }
      
    })
  }
  
  func downloadExtraImages() {
    self.extra_images.removeAll()
    for imageURL in currentEvent.extraImagesURL
    {
      DataService.dataService.downloadExtraImage(for: currentEvent, imageURL: imageURL, with: { (image) in
        self.extra_images.append(ImageSource(image: image))
      })
    }
  }
  
  func setInfoTextForEvent() {
    startDateText.text = currentEvent.start.dateOutput(format: kDefaultDateFormat)
    endDateText.text = currentEvent.end.dateOutput(format: kDefaultDateFormat)
    lastModifiedText.text = currentEvent.lastModified.dateOutput(format: kDefaultDateFormat)
    
    imageView.image = currentEvent.mainImage
    descriptionText.text = currentEvent.info
    nameText.text = currentEvent.title
    addressText.text = currentEvent.address
    likesText.text = currentEvent.likes.description
    if currentEvent.price == 0.0 { priceText.text = "Free".localized() }
    else                         { priceText.text = currentEvent.price.description + NSLocale.current.currencySymbol! }
    ticketsButton.setTitle(currentEvent.ticketsURL?.urlDescription(), for: .normal)
    websiteButton.setTitle(currentEvent.website?.urlDescription(), for: .normal)
  }
  
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
    pageControl.currentPage = Int(pageNumber)
  }
  
  @IBAction func liked(_ sender: UIBarButtonItem) {
    sender.image = sender.image == kUnlikedImage ? kLikedImage : kUnlikedImage
    likeEvent()
  }
  
  func likeEvent() {
    DataService.dataService.like(event: currentEvent, with: WUser.sharedInstance, completion: nil)
  }
  
  @IBAction func searchWeb(_ sender: UIButton) {
    if let url = URL(string: sender.currentTitle!) {
      if UIApplication.shared.canOpenURL(url) {
        if #available(iOS 10.0, *) {
          UIApplication.shared.open(url)
        } else {
          UIApplication.shared.openURL(url)
        }
      }
      else {
        print("Not a correct URL")
      }
    }
    
  }
  
  @IBAction func viewPhotos(_ sender: UIButton) {
    if currentEvent.extraImages.count > 0 {
      let agrume = Agrume(images: currentEvent.extraImages, startIndex: 0, backgroundBlurStyle: .light)
      agrume.showFrom(self)
    } else {
      presentDefaultAlert(title: NSLocalizedString("Lo sentimos", comment: ""), message: NSLocalizedString("No existen imágenes para este evento", comment: ""))
    }
  }
  @IBAction func imageTapped(_ sender: UITapGestureRecognizer) {
    if let eventImage = currentEvent.mainImage {
      let agrume = Agrume(image: eventImage)
      agrume.showFrom(self)
    }
  }
  
  @IBAction func close(_ sender: UIBarButtonItem) {
    self.dismiss(animated: true, completion: nil)
  }

  @IBAction func shareImageButton(_ sender: UIBarButtonItem) {
    let text  = currentEvent.info
    let dataToShare: [Any]
    
    if let image = currentEvent.mainImage {
      dataToShare = [image, text]
    } else {
      dataToShare = [text]
    }
    
    let activityViewController = UIActivityViewController(activityItems: dataToShare, applicationActivities: nil)
    activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
    activityViewController.excludedActivityTypes = [ UIActivityType.airDrop]
    
    self.present(activityViewController, animated: true, completion: nil)
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == kShowEditEvent {
      let vc = segue.destination as! EditEventViewController
      vc.event = currentEvent
    }
  }
}
