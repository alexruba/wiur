//
//  RegisterViewController.swift
//  Wiur
//
//  Created by Oxthor on 13/01/2017.
//  Copyright © 2017 Rubio. All rights reserved.
//

import Foundation
import Firebase

class RegisterViewController: UIViewController, UITextFieldDelegate
{
  
  @IBOutlet weak var nameTextField: UITextField!
  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var usernameTextField: UITextField!
  @IBOutlet weak var repeatedPasswordTextField: UITextField!
  
  @IBOutlet weak var userImage: UIImageView!
  @IBOutlet weak var teamImage: UIImageView!
  
  var userType : UserType? = nil
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.hideKeyboardWhenTappedAround()
    
    self.emailTextField.delegate = self
    self.passwordTextField.delegate = self
    self.usernameTextField.delegate = self
    self.nameTextField.delegate = self
    self.repeatedPasswordTextField.delegate = self
    
    setupUserTypeImages()
    
    navigationController?.setNavigationBarHidden(false, animated: true)
  }
  
  func setupUserTypeImages() {
    let userTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(RegisterViewController.userImageTapped(_:)))
    let teamTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(RegisterViewController.userImageTapped(_:)))
    userImage.addGestureRecognizer(userTapRecognizer)
    teamImage.addGestureRecognizer(teamTapRecognizer)
  }
  
  @IBAction func register(_ sender: UIButton) {
    guard let email = emailTextField.text, !email.isEmpty,
          let password = passwordTextField.text, !password.isEmpty,
          let username = usernameTextField.text, !username.isEmpty,
          let name = nameTextField.text, !name.isEmpty,
          let repeatedPassword = repeatedPasswordTextField.text, !repeatedPassword.isEmpty,
          let userType = userType else {
        self.presentDefaultAlert(title: "Error", message: "Needed data is missing".localized())
        return
    }
    
    if (repeatedPassword != password) {
      self.presentDefaultAlert(title: "Error", message: "Passwords do not match".localized())
    } else {
      let user = WUser()
      user.email = email
      user.username = username
      user.name = name
      user.type = userType
      
      DataService.dataService.registerUser(newUser: user, password: password, with: { 
        let mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateInitialViewController()
        self.present(vc!, animated: true, completion: nil)
      }, failure: { error in
        self.presentDefaultAlert(title: "Error", message: error)
      })
    }
  }
  
  func userImageTapped(_ sender: UITapGestureRecognizer) {
    if userType == UserType.user {
      userImage.image = kUserNotSelectedRegisterImage
      teamImage.image = kTeamSelectedRegisterImage
      
      userType = UserType.team
    } else {
      userImage.image = kUserSelectedRegisterImage
      teamImage.image = kTeamNotSelectedRegisterImage
      
      userType = UserType.user
    }
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    self.view.endEditing(true)
    return false
  }
}
