//
//  LoginViewController.swift
//  Wiur
//
//  Created by Oxthor on 13/01/2017.
//  Copyright © 2017 Rubio. All rights reserved.
//

import Foundation
import Firebase

class LoginViewController: UIViewController, UITextFieldDelegate {
  
  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var noAccountButton: UIButton!
  @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.hideKeyboardWhenTappedAround()
    navigationController?.setNavigationBarHidden(false, animated: true)
    
    self.emailTextField.delegate = self
    self.passwordTextField.delegate = self
    noAccountButton.titleLabel?.adjustsFontSizeToFitWidth = true
  }
  
  @IBAction func login(_ sender: UIButton) {
    guard let email = self.emailTextField.text, !email.isEmpty,
          let password = self.passwordTextField.text, !password.isEmpty else {
        self.presentDefaultAlert(title: "Error", message: "Please, try again with a correct email and password".localized())
        return
    }
    
    DataService.dataService.signIn(email: email, password: password, with: { 
      let loginStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
      let vc = loginStoryboard.instantiateInitialViewController()
      self.present(vc!, animated: true, completion: nil)
    }) { error in
      self.presentDefaultAlert(title: "Error", message: error)
    }
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    self.view.endEditing(true)
    return false
  }
}
