//
//  NewsViewController.swift
//  Wiur
//
//  Created by Oxthor on 01/12/2016.
//  Copyright © 2016 Rubio. All rights reserved.
//

import UIKit
import Firebase
import Agrume
import MapKit
import CoreLocation

class FeedViewController: UICollectionViewController, CategoryTableViewDelegate, CLLocationManagerDelegate, SettingsDelegate
{
  @IBOutlet var updateButton: UIButton!
  
  var events = [[Event]]()
  var sections = ["This week".localized(),
                  "Upcoming".localized(),
                  "Finished".localized()]
  var activityIndicatorView = UIActivityIndicatorView()
  var initialDataLoaded = false
  var queryRef: DatabaseQuery = DatabaseQuery()
  var filtering : Bool = false
  let refreshControl: UIRefreshControl = UIRefreshControl()
  
  let locationManager = CLLocationManager()
  let regionRadius: CLLocationDistance = 2000
  var currentLocation = CLLocation()
  var currentDistance: Double = 0.0
  var currentQuery: DatabaseQuery = DatabaseQuery()
  
  @IBOutlet weak var reloadButton: UIBarButtonItem!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    startLocationManager()
    
    collectionView?.dataSource = self
    collectionView?.delegate = self
    
    queryRef = DataService.dataService.EVENT_REF
    currentQuery = queryRef
    loadEventsFromDatabaseWithQuery(query:queryRef)
    
    collectionView?.register(UINib(nibName: "EventCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "EventCell")
    
    setupTableViewElements()
    setDefaultNavigationController()
    setupCurrentLocation()
    
  }
  
  override func viewDidLayoutSubviews() {
    setupUpdateButton()
  }
  
  func setupCurrentLocation() {
    let currentLatitude = UserDefaults.standard.float(forKey: kCurrentLocationLatitude)
    let currentLongitude = UserDefaults.standard.float(forKey: kCurrentLocationLongitude)
    currentLocation = CLLocation(latitude: CLLocationDegrees(currentLatitude), longitude: CLLocationDegrees(currentLongitude))
    currentDistance = UserDefaults.standard.double(forKey: kDistanceFromUserPositionKey)
  }
  
  // MARK: Current Location
  func startLocationManager() {
    // Ask for Authorisation from the User.
    locationManager.requestAlwaysAuthorization()
    
    // For use in foreground
    locationManager.requestWhenInUseAuthorization()
    
    if CLLocationManager.locationServicesEnabled() {
      locationManager.delegate = self
      locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
      locationManager.startUpdatingLocation()
    }
  }
  
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    if (UserDefaults.standard.object(forKey: kCurrentLocationLatitude) == nil) {
      currentLocation = manager.location!
      currentDistance = regionRadius
      UserDefaults.standard.set(currentLocation.coordinate.latitude, forKey: kCurrentLocationLatitude)
      UserDefaults.standard.set(currentLocation.coordinate.longitude, forKey: kCurrentLocationLongitude)
      UserDefaults.standard.set(currentDistance, forKey: kDistanceFromUserPositionKey)
    }
  }
  
  func didSelectCategory(category: String) {
    filter(category: category)
  }
  
  override func scrollViewDidScroll(_ scrollView: UIScrollView) {
    let currentOffset = scrollView.contentOffset
    let yOffset = currentOffset.y
    
    if yOffset < -120.0 && !(refreshControl.isRefreshing) {
      refreshControl.beginRefreshing()
      handleRefresh(refreshControl: refreshControl)
    }
  }
  
  func handleRefresh(refreshControl: UIRefreshControl) {
    let delayInSeconds = 3.0;
    DispatchQueue.main.asyncAfter(deadline: .now() + delayInSeconds) {
      self.refreshControl.endRefreshing()
      self.updateTableWithAnimation()
    }
  }
  
  func updateTableWithAnimation() {
    let range = NSMakeRange(0, (self.collectionView?.numberOfSections)!)
    let sections = NSIndexSet(indexesIn: range)
    self.collectionView?.reloadSections(sections as IndexSet)
  }
  
  
  // MARK: Settings Delegate
  func didUpdateDistance() {
    let latitude = UserDefaults.standard.float(forKey: kCurrentLocationLatitude)
    let longitude = UserDefaults.standard.float(forKey: kCurrentLocationLongitude)
    
    currentLocation = CLLocation(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude))
    currentDistance = UserDefaults.standard.double(forKey: kDistanceFromUserPositionKey)
    
    loadEventsFromDatabaseWithQuery(query: currentQuery)
  }
  
  // MARK: Actions
  func filter(category: String) {
    filtering = true
    reloadButton.tintColor = UIColor.black
    reloadButton.isEnabled = true
    
    currentQuery = queryRef.queryOrdered(byChild: "category").queryEqual(toValue: category)
    loadEventsFromDatabaseWithQuery(query: currentQuery)
  }
  
  @IBAction func reload(_ sender: UIBarButtonItem) {
    filtering = false
    reloadButton.tintColor = UIColor.clear
    reloadButton.isEnabled = false
    
    currentQuery = queryRef
    loadEventsFromDatabaseWithQuery(query: currentQuery)
  }
  
  @IBAction func showCategoryTable(_ sender: UIBarButtonItem) {
    let nav = UIStoryboard.init(name: "CategoryTable", bundle: nil).instantiateInitialViewController() as! UINavigationController
    let vc = nav.viewControllers.first as! CategoryTableViewController
    vc.delegate = self
    self.present(nav, animated: true, completion: nil)
  }
  
  @IBAction func update(_ sender: UIButton) {
    UIView.animate(withDuration: 0.4, animations: {
      self.updateButton.alpha = 0
      self.collectionView?.setContentOffset(CGPoint.zero, animated: true)
      self.updateTableWithAnimation()
    }, completion: {
      (finished: Bool) in
      self.updateButton.isHidden = true
    })
  }
  
  func showUpdateButtonWithAnimation() {
    updateButton.isHidden = false
    UIView.animate(withDuration: 0.25, animations: {
      self.updateButton.alpha = 1
    }, completion: {
      (finished: Bool) in
    })
  }
  
  func indexFor(event: Event) -> IndexPath {
    for section in 0..<sections.count {
      for row in 0..<events[section].count {
        if event.uid == events[section][row].uid {
          return IndexPath(row: row, section: section)
        }
      }
    }
    
    return IndexPath()
  }
  
  
  func setupTableViewElements() {
    // Pull to refresh
    refreshControl.addTarget(self, action: #selector(FeedViewController.handleRefresh(refreshControl:)), for: .valueChanged)
    refreshControl.tintColor = UIColor.brown
    self.collectionView?.addSubview(refreshControl)
    
    collectionView?.alwaysBounceVertical = true;
    
    activityIndicatorView.center = self.view.center
    activityIndicatorView.startAnimating()
    activityIndicatorView.hidesWhenStopped = true
    activityIndicatorView.color = UIColor.brown
    collectionView?.addSubview(activityIndicatorView)
    collectionView?.bringSubview(toFront: activityIndicatorView)
    
    reloadButton.tintColor = UIColor.clear
    reloadButton.isEnabled = false
  }
  
  func setupUpdateButton() {
    updateButton.rounded(cornerRadius: 10.0, borderWidth: 0.0, borderColor: .clear)
    updateButton.alpha = 0.0
    updateButton.titleLabel?.adjustsFontSizeToFitWidth = true
    updateButton.translatesAutoresizingMaskIntoConstraints = false
    
    guard let collectionView = collectionView else { return }
    collectionView.addSubview(updateButton)
    collectionView.bringSubview(toFront: updateButton)
    
    NSLayoutConstraint.activate([
      updateButton.topAnchor.constraint(equalTo: collectionView.topAnchor, constant: 8.0),
      updateButton.centerXAnchor.constraint(equalTo: collectionView.centerXAnchor),
      updateButton.heightAnchor.constraint(equalTo: collectionView.heightAnchor, multiplier: 0.05)
    ])
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == kEventDetailSegue {
      if let viewController = segue.destination as? UINavigationController {
        let indexPath = self.collectionView?.indexPathsForSelectedItems?.first
        let event = events[(indexPath?.section)!][(indexPath?.row)!]
        let vc = viewController.viewControllers.first as! DetailViewController
        vc.currentEvent = event
      }
    }
    
    if segue.identifier == kShowSettings {
      if let viewController = segue.destination as? UINavigationController {
        let vc = viewController.viewControllers.first as! SettingsViewController
        vc.settingsDelegate = self
      }
    }
  }

}
