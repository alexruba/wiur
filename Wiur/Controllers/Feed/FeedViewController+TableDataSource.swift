//
//  FeedViewController+DataSource.swift
//  Wiur
//
//  Created by Oxthor on 22/02/2017.
//  Copyright © 2017 Rubio. All rights reserved.
//

import Foundation
import UIKit

extension FeedViewController
{
  override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    performSegue(withIdentifier: kEventDetailSegue, sender: nil)
  }
  
  override func numberOfSections(in collectionView: UICollectionView) -> Int {
    return sections.count
  }
  
  override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return events[section].count
  }
  
  override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
    let reusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "DateHeaderView", for: indexPath)
    
    reusableView.layer.masksToBounds = false
    reusableView.layer.shadowColor = UIColor.lightGray.cgColor
    reusableView.layer.shadowOpacity = 0.8
    reusableView.layer.shadowOffset = CGSize(width: 0, height: 2.0)
    reusableView.layer.shadowRadius = 2
    
    let headerLabel = reusableView.viewWithTag(1) as! UILabel
    headerLabel.text = sections[indexPath.section]
    
    return reusableView
  }
  
  override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EventCell", for: indexPath) as! EventCollectionViewCell
    let currentEvent = events[indexPath.section][indexPath.row]
    cell.configure(with: currentEvent)
    
    return cell
  }
}
