//
//  FeedViewController+FirebaseData.swift
//  Wiur
//
//  Created by Oxthor on 22/02/2017.
//  Copyright © 2017 Rubio. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import MapKit

extension FeedViewController
{
  // MARK: Firebase events
  func loadEventsFromDatabaseWithQuery(query: DatabaseQuery) {
    initialDataLoaded = false
    events = Array(repeating: [Event](), count: sections.count)
    activityIndicatorView.startAnimating()
    DataService.dataService.EVENT_REF.removeAllObservers()
    
    query.observe(.childChanged, with: {
      (snapshot) in
      let event = Event(snapshot: snapshot)
      let eventIndexPath = self.indexFor(event: event)
      let location = CLLocation(latitude: event.coordinate.latitude, longitude: event.coordinate.longitude)
      
      if location.distance(from: self.currentLocation) < self.currentDistance {
        event.mainImage = self.events[eventIndexPath.section][eventIndexPath.row].mainImage
        self.events[eventIndexPath.section][eventIndexPath.row] = event
      } else {
        self.removeEvent(event: event)
      }
    })
    
    query.observe(.childAdded, with: { snapshot in
        let event = Event(snapshot: snapshot)
        let location = CLLocation(latitude: event.coordinate.latitude, longitude: event.coordinate.longitude)
        
        if location.distance(from: self.currentLocation) < self.currentDistance {
          self.addEvent(event: event)
          
          if self.initialDataLoaded {
            self.showUpdateButtonWithAnimation()
          }
        }
    })
    
    query.observe(.childRemoved, with: { snapshot in
      let event = Event(snapshot: snapshot)
      let location = CLLocation(latitude: event.coordinate.latitude, longitude: event.coordinate.longitude)
      
      if location.distance(from: self.currentLocation) < self.currentDistance {
        self.removeEvent(event: event)
      }
      
    })
    
    query.observeSingleEvent(of: .value, with: { snapshot in
      self.initialDataLoaded = true
      self.updateTableWithAnimation()
      self.activityIndicatorView.stopAnimating()
    })
  }

  func removeEvent(event: Event) {
    let eventIndexPath = self.indexFor(event: event)
    let cell = self.collectionView?.cellForItem(at: eventIndexPath)
    
    UIView.animate(withDuration: 0.75, animations: {
      cell?.alpha = 0
    }, completion: { finished in
      self.events[eventIndexPath.section].remove(at: eventIndexPath.row)
      self.updateTableWithAnimation()
    })
  }

  func addEvent(event: Event) {
    if event.range() == .thisweek {
      events[0].append(event)
      events[0].sort(by: {$0.likes > $1.likes})
    } else if event.range() == .upcoming {
      events[1].append(event)
      events[1].sort(by: {$0.likes > $1.likes})
    } else {
      events[2].append(event)
      events[2].sort(by: {$0.likes > $1.likes})
    }
    
    DataService.dataService.downloadMainImage(for: event) { 
      let eventIndexPath = self.indexFor(event: event)
      if let cell = self.collectionView?.cellForItem(at: eventIndexPath) as? EventCollectionViewCell {
        cell.eventImage.image = event.mainImage
        cell.imageActivityIndicatorView.stopAnimating()
      }
    }
  }
}
