//
//  ExtensionDate.swift
//  Wiur
//
//  Created by Oxthor on 19/12/2016.
//  Copyright © 2016 Rubio. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore
import Firebase

extension Date {
  func isBetweeen(date date1: Date, andDate date2: Date) -> Bool {
    return date1 < self && date2 > self
  }
  
  func getWeekAgoDay() -> Date {
    return self.addingTimeInterval(-7*24*60*60)
  }
  
  func isEarlier(date: Date) -> Bool {
    return self < date
  }
  
  func isLater(date: Date) -> Bool {
    return self > date
  }
  
  func dateOutput(format: String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = format
    return dateFormatter.string(from: self)
  }
}

extension String {
  func urlDescription() -> String {
    if self == "" {
      return "No definida"
    } else {
      if let url = URL(string: self) {
        if UIApplication.shared.canOpenURL(url) {
          return self
        }
        else {
          return "URL no correcta"
        }
      }
      else {
        return "URL no correcta"
      }
    }
  }
}

extension UIColor {
  convenience init(red: Int, green: Int, blue: Int) {
    assert(red >= 0 && red <= 255, "Invalid red component")
    assert(green >= 0 && green <= 255, "Invalid green component")
    assert(blue >= 0 && blue <= 255, "Invalid blue component")
    
    self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
  }
  
  convenience init(netHex:Int) {
    self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
  }
}

extension Double {
  /// Rounds the double to decimal places value
  func roundTo(places:Int) -> Double {
    let divisor = pow(10.0, Double(places))
    return (self * divisor).rounded() / divisor
  }
}

extension UINavigationItem {
  func setTitleImage(image: UIImage) {
    let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
    imageView.contentMode = .scaleAspectFit
    imageView.image = image
    self.titleView = imageView
  }
}

extension CAGradientLayer {
  class func gradientLayerForBounds(bounds: CGRect, startColor: UIColor, endColor: UIColor) -> CAGradientLayer {
    let layer = CAGradientLayer()
    layer.frame = bounds
    layer.colors = [startColor.cgColor, endColor.cgColor]
    return layer
  }
}

extension UIImage {
  class func imageLayerForGradientBackground(bounds: CGRect, startColor: UIColor, endColor: UIColor) -> UIImage {
    var updatedFrame = bounds
    // take into account the status bar
    updatedFrame.size.height += 20
    let layer = CAGradientLayer.gradientLayerForBounds(bounds: updatedFrame, startColor: startColor, endColor: endColor)
    UIGraphicsBeginImageContext(layer.bounds.size)
    layer.render(in: UIGraphicsGetCurrentContext()!)
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return image!
  }
  
  class func imageWithColor(color: UIColor) -> UIImage {
    let rect = CGRect(origin: CGPoint(x: 0, y:0), size: CGSize(width: 1, height: 1))
    UIGraphicsBeginImageContext(rect.size)
    let context = UIGraphicsGetCurrentContext()!
    
    context.setFillColor(color.cgColor)
    context.fill(rect)
    
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return image!
  }
}

extension UIView {
  func rounded(cornerRadius: CGFloat, borderWidth: CGFloat, borderColor: UIColor) {
    self.layer.cornerRadius = cornerRadius
    self.layer.borderWidth = borderWidth
    self.layer.borderColor = borderColor.cgColor
    self.clipsToBounds = true
  }
}

extension UIViewController {
  func hideKeyboardWhenTappedAround() {
    let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
    tap.cancelsTouchesInView = false
    view.addGestureRecognizer(tap)
  }
  
  func dismissKeyboard() {
    view.endEditing(true)
  }
  
  func animateIn(viewToAnimate: UIView, visualEffect: UIVisualEffect, visualEffectView: UIVisualEffectView) {
    viewToAnimate.isHidden = false
    viewToAnimate.center = self.view.center
    
    viewToAnimate.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
    viewToAnimate.alpha = 0
    
    UIView.animate(withDuration: 0.4) {
      visualEffectView.effect = visualEffect
      viewToAnimate.alpha = 1
      viewToAnimate.transform = CGAffineTransform.identity
    }
  }
  func animateOut(viewToAnimate: UIView, visualEffectView: UIVisualEffectView) {
    UIView.animate(withDuration: 0.3, animations: {
        viewToAnimate.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        viewToAnimate.alpha = 0
        visualEffectView.effect = nil
    }) { _ in
      viewToAnimate.isHidden = true
    }
  }
  
  func presentDefaultAlert(title: String, message: String) {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    
    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
    alertController.addAction(defaultAction)
    
    self.present(alertController, animated: true, completion: nil)
  }
  
  func setDefaultNavigationController() {
    // Navigation bar color
    let startColor = kWiurLightGreyColor
    let endColor = kWiurLightGreyColor
    self.extendedLayoutIncludesOpaqueBars = true
    self.navigationController?.navigationBar.isTranslucent = false
    let fontDictionary = [ NSForegroundColorAttributeName:UIColor.black ]
    self.navigationController?.navigationBar.titleTextAttributes = fontDictionary
    self.navigationController?.navigationBar.setBackgroundImage(UIImage.imageLayerForGradientBackground(bounds: (navigationController?.navigationBar.bounds)!, startColor: startColor, endColor: endColor), for: UIBarMetrics.default)
    
    self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back".localized(), style:.plain, target:nil, action:nil)
    
    self.navigationController?.setNavigationBarHidden(false, animated: true)
  }
}

extension UIImageView {
  func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
    contentMode = mode
    URLSession.shared.dataTask(with: url) { (data, response, error) in
      guard
        let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
        let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
        let data = data, error == nil,
        let image = UIImage(data: data)
        else { return }
      DispatchQueue.main.async() { _ in
        self.image = image
      }
    }.resume()
  }
  
  func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
    guard let url = URL(string: link) else { return }
    downloadedFrom(url: url, contentMode: mode)
  }
}

extension String {
  func localized() -> String {
    return NSLocalizedString(self, comment: "")
  }
}
