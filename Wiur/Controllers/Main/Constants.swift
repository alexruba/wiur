//
//  Constants.swift
//  Wiur
//
//  Created by Oxthor on 19/02/2017.
//  Copyright © 2017 Rubio. All rights reserved.
//

import Foundation
import UIKit

enum UserType: Int {
  case user
  case team
}

// Segues, embeds, modals
let kEventDetailSegue = "show-event-detail"
let kEmbedAccountOptions = "embed-account-options"
let kShowUserEvents = "show-user-events"
let kShowUserFavorites = "show-user-favorites"
let kShowEditEvent = "show-edit-event"
let kShowSettings = "show-settings"

// Cells
let kCellEventAddressPriceDateLikes = "EventAddressPriceDateLikesCell"
let kCellEventStatusInfo = "EventAddressPriceDateLikesCell"

// Utilities
let kDefaultDateFormat = "EEEE, d MMMM, HH:mm"
let kDateFormatFirebase = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

// Images
let kUnlikedImage = #imageLiteral(resourceName: "like")
let kLikedImage = #imageLiteral(resourceName: "liked")
let kExtraImagePlaceholder = #imageLiteral(resourceName: "image-template")
let kMainImagePlaceholder = #imageLiteral(resourceName: "main-image-template")
let kUserPlaceholderImage = #imageLiteral(resourceName: "DefaultAvatar")
let kUserNotSelectedRegisterImage = #imageLiteral(resourceName: "usuario")
let kUserSelectedRegisterImage = #imageLiteral(resourceName: "user-selected")
let kTeamNotSelectedRegisterImage = #imageLiteral(resourceName: "organizador")
let kTeamSelectedRegisterImage = #imageLiteral(resourceName: "users-selected")

// User defaults
let kDistanceFromUserPositionKey = "distance-from-user"
let kCurrentLocationStreet = "current-location-street"
let kCurrentLocationLatitude = "current-location-latitude"
let kCurrentLocationLongitude = "current-location-longitude"

// Colors
let kWiurDefaultColor = UIColor.init(red: 159/255, green: 119/255, blue: 98/255, alpha: 255/255)
let kWiurRoseBrownColor = UIColor.init(red: 141/255, green: 105/255, blue: 103/255, alpha: 255/255)
let kWiurLightGreyColor = UIColor.init(red: 240/255, green: 240/255, blue: 240/255, alpha: 255/255)
