//
//  BlurryTabBar.swift
//  Wiur
//
//  Created by Oxthor on 26/01/2017.
//  Copyright © 2017 Rubio. All rights reserved.
//

import UIKit

class BlurryTabBar: UITabBar {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.tintColor = kWiurDefaultColor
        if #available(iOS 10.0, *) {
            self.unselectedItemTintColor = UIColor.black
        } else {
            self.barTintColor = UIColor.white
        }
    }
}

