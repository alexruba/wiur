//
//  SettingsViewController+MapView.swift
//  Wiur
//
//  Created by Oxthor on 07/03/2017.
//  Copyright © 2017 Rubio. All rights reserved.
//

import MapKit

extension SettingsViewController {
  func centerMapOnLocation(location: CLLocation) {
    let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, currentDistance * 2.0, currentDistance * 2.0)
    mapView.setRegion(coordinateRegion, animated: true)
  }
}
