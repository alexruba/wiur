//
//  SettingsViewController.swift
//  Wiur
//
//  Created by Oxthor on 07/03/2017.
//  Copyright © 2017 Rubio. All rights reserved.
//

import Foundation
import UIKit
import MapKit

protocol SettingsDelegate {
  func didUpdateDistance()
}

class SettingsViewController: UITableViewController, SetMapPositionDelegate {
  var currentLocation: CLLocation = CLLocation()
  var currentDistance: Double = 0.0
  var settingsDelegate: SettingsDelegate? = nil
  
  @IBOutlet weak var distanceSliderValueLabel: UILabel!
  @IBOutlet weak var distanceSlider: UISlider!
  @IBOutlet weak var mapView: MKMapView!
  
  // MARK: View lifecycle
  override func viewDidLoad() {
    let latitude = UserDefaults.standard.float(forKey: kCurrentLocationLatitude)
    let longitude = UserDefaults.standard.float(forKey: kCurrentLocationLongitude)
    currentLocation = CLLocation(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude))
    currentDistance = UserDefaults.standard.double(forKey: kDistanceFromUserPositionKey)
    
    updateDistanceValues()
    centerMapOnLocation(location: currentLocation)
  }
  
  // MARK: UI
  func updateDistanceValues() {
    distanceSlider.value = Float((currentDistance / 1000).roundTo(places: 2))
    distanceSliderValueLabel.text = distanceSlider.value.description + "km"
  }
  
  
  // MARK: SetMapPosition Delegate
  func didSelectMapPosition(position: CLPlacemark) {
    currentLocation = position.location!
    centerMapOnLocation(location: currentLocation)
  }
  
  
  // MARK: Actions
  @IBAction func mapViewTapped(_ sender: UITapGestureRecognizer) {
    let vc = UIStoryboard.init(name: "SetMapPosition", bundle: nil).instantiateInitialViewController() as! SetMapPositionViewController
    vc.setMapPositionDelegate = self
    
    self.navigationController?.pushViewController(vc, animated: true)
  }
  
  @IBAction func close(_ sender: UIBarButtonItem) {
    UserDefaults.standard.set(currentDistance, forKey: kDistanceFromUserPositionKey)
    UserDefaults.standard.set(currentLocation.coordinate.latitude, forKey: kCurrentLocationLatitude)
    UserDefaults.standard.set(currentLocation.coordinate.longitude, forKey: kCurrentLocationLongitude)
    
    settingsDelegate?.didUpdateDistance()
    
    dismiss(animated: true, completion: nil)
  }
  
  @IBAction func sliderValueChanged(_ sender: UISlider) {
    distanceSliderValueLabel.text = distanceSlider.value.description + "km"
    currentDistance = Double(distanceSlider.value * 1000)
    let coordinateRegion = MKCoordinateRegionMakeWithDistance(self.currentLocation.coordinate, currentDistance * 2.0, currentDistance * 2.0)
    self.mapView.setRegion(coordinateRegion, animated: true)
  }
  
}
