//
//  SetMapPositionViewController.swift
//  Wiur
//
//  Created by Oxthor on 23/01/2017.
//  Copyright © 2017 Rubio. All rights reserved.
//

import Foundation
import MapKit
import UIKit

protocol HandleMapSearch {
  func dropPinZoomIn(placemark:MKPlacemark)
}

protocol SetMapPositionDelegate {
  func didSelectMapPosition(position: CLPlacemark)
}

class SetMapPositionViewController: UIViewController, CLLocationManagerDelegate {
  @IBOutlet weak var mapView: MKMapView!
  let regionRadius: CLLocationDistance = 1000
  let locationManager = CLLocationManager()
  
  var resultSearchController: UISearchController? = nil
  var selectedPin:MKPlacemark? = nil
  var setMapPositionDelegate: SetMapPositionDelegate? = nil
  
  override func viewDidLoad() {
    startLocationManager()
    setResultSearchController()
  }
  
  func setResultSearchController() {
    let locationSearchTable = storyboard!.instantiateViewController(withIdentifier: "LocationSearchTable") as! LocationSearchTable
    resultSearchController = UISearchController(searchResultsController: locationSearchTable)
    resultSearchController?.searchResultsUpdater = locationSearchTable
    
    let searchBar = resultSearchController!.searchBar
    searchBar.sizeToFit()
    searchBar.placeholder = "Search for places".localized()
    navigationItem.titleView = resultSearchController?.searchBar
    
    resultSearchController?.hidesNavigationBarDuringPresentation = false
    resultSearchController?.dimsBackgroundDuringPresentation = true
    definesPresentationContext = true
    
    locationSearchTable.mapView = mapView
    locationSearchTable.handleMapSearchDelegate = self
  }
  
  // MARK: Current Location
  func startLocationManager() {
    // Ask for Authorisation from the User.
    locationManager.requestAlwaysAuthorization()
    
    // For use in foreground
    locationManager.requestWhenInUseAuthorization()
    
    if CLLocationManager.locationServicesEnabled() {
      locationManager.delegate = self
      locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
      locationManager.startUpdatingLocation()
    }
  }
  
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    centerMapOnLocation(location: manager.location!)
    locationManager.delegate = nil
  }
  
  func centerMapOnLocation(location: CLLocation) {
    let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius * 2.0, regionRadius * 2.0)
    mapView.setRegion(coordinateRegion, animated: true)
  }
}

extension SetMapPositionViewController: HandleMapSearch {
  func dropPinZoomIn(placemark:MKPlacemark){
    selectedPin = placemark
    mapView.removeAnnotations(mapView.annotations)
    
    let annotation = MKPointAnnotation()
    annotation.coordinate = placemark.coordinate
    annotation.title = placemark.name
    
    if let city = placemark.locality,
      let state = placemark.administrativeArea {
      annotation.subtitle = "\(city) \(state)"
    }
    
    mapView.addAnnotation(annotation)
    let span = MKCoordinateSpanMake(0.05, 0.05)
    let region = MKCoordinateRegionMake(placemark.coordinate, span)
    mapView.setRegion(region, animated: true)
    
    setMapPositionDelegate?.didSelectMapPosition(position: placemark)
  }
}
