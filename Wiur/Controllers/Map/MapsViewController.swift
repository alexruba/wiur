//
//  MapsViewController.swift
//  Wiur
//
//  Created by Oxthor on 01/12/2016.
//  Copyright © 2016 Rubio. All rights reserved.
//

import UIKit
import MapKit
import CoreGraphics
import SwiftyJSON
import Firebase
import CoreLocation
import Agrume

class MapsViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate, CategoryTableViewDelegate, SettingsDelegate
{
  @IBOutlet weak var mapView: MKMapView!
  @IBOutlet weak var visualEffectView: UIVisualEffectView!
  @IBOutlet weak var titleDetailView: UILabel!
  @IBOutlet weak var imageDetailView: UIImageView!
  @IBOutlet weak var detailView: UIView!
  @IBOutlet weak var detailInfoView: UIView!
  @IBOutlet weak var refreshButton: UIBarButtonItem!
  @IBOutlet weak var filterButton: UIBarButtonItem!
  @IBOutlet weak var categoryContainerView: UIView!
  
  let regionRadius: CLLocationDistance = 1000
  var effect: UIVisualEffect!
  var currentEvent = Event()
  let locationManager = CLLocationManager()
  var queryRef :DatabaseQuery = DatabaseQuery()
  var currentLocation: CLLocation = CLLocation()
  
  // MARK: View lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setupCategoryContainerView()
    setupViewStyle()
    
    mapView.delegate = self
    mapView.showsUserLocation = true;
    
    queryRef = DataService.dataService.EVENT_REF
    
    loadEventsWithQuery(query: queryRef)
    
    setCurrentLocation()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    setDefaultNavigationController()
  }
  
  func setupCategoryContainerView() {
    let vc = UIStoryboard.init(name: "CategoryTable", bundle: nil).instantiateViewController(withIdentifier: "CategoryTable") as! CategoryTableViewController
    self.addChildViewController(vc)
    vc.view.frame = CGRect(x: 0, y: 0, width: self.categoryContainerView.frame.size.width, height: self.categoryContainerView.frame.size.height);
    self.categoryContainerView.addSubview(vc.view)
    vc.delegate = self
    vc.didMove(toParentViewController: self)
  }
  
  func setupViewStyle() {
    self.view.layoutIfNeeded()
    
    effect = visualEffectView.effect
    visualEffectView.effect = nil
    
    imageDetailView.rounded(cornerRadius: 10.0, borderWidth: 5.0, borderColor: UIColor.white)
    detailInfoView.rounded(cornerRadius: 10.0, borderWidth: 1.0, borderColor: view.tintColor)
    categoryContainerView.rounded(cornerRadius: 10.0, borderWidth: 1.0, borderColor: view.tintColor)
  }
  
  // MARK: CategoryTableViewDelegate
  func didSelectCategory(category: String) {
    loadEventsWithQuery(query: queryRef.queryOrdered(byChild: "category").queryEqual(toValue: category))
    animateOut(viewToAnimate: categoryContainerView, visualEffectView: visualEffectView)
  }
  
  // MARK: UI Actions
  @IBAction func routeToLocation(_ sender: UIButton) {
    let coordinate = CLLocationCoordinate2DMake(currentEvent.coordinate.latitude, currentEvent.coordinate.longitude)
    let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
    mapItem.name = currentEvent.title!
    mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
  }
  
  @IBAction func dismissDetail(_ sender: Any) {
    self.animateOut(viewToAnimate: detailView, visualEffectView: visualEffectView)
    filterButton.isEnabled = true
    refreshButton.isEnabled = true
  }
  
  @IBAction func queryData(_ sender: UIBarButtonItem) {
    if !categoryContainerView.isHidden {
      self.animateOut(viewToAnimate: categoryContainerView, visualEffectView: visualEffectView)
    }
    else {
      self.animateIn(viewToAnimate: categoryContainerView, visualEffect: effect, visualEffectView: visualEffectView)
    }
  }
  
  @IBAction func refreshData(_ sender: UIBarButtonItem) {
    DataService.dataService.BASE_REF.removeAllObservers()
    loadEventsWithQuery(query: queryRef)
  }
  
  
  // MARK: Firebase queries
  func loadEventsWithQuery(query: DatabaseQuery) {
    self.mapView.removeAnnotations(self.mapView.annotations)
    
    DataService.dataService.BASE_REF.removeAllObservers()
    
    query.observe(.childAdded, with: { snapshot in
      let event = Event(snapshot: snapshot)
      DataService.dataService.downloadMainImage(for: event, with: {
        self.mapView.addAnnotation(event)
      })
    })
    
    query.observe(.childRemoved, with: { snapshot in
      let event = Event(snapshot: snapshot)
      self.mapView.removeAnnotation(self.annotationFor(event: event))
    })
  }
  
  // MARK: Segue
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if let viewController = segue.destination as? UINavigationController {
      let vc = viewController.viewControllers.first as! DetailViewController
      vc.currentEvent = currentEvent
    }
  }
}
