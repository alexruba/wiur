//
//  MapsViewController+MapView.swift
//  Wiur
//
//  Created by Oxthor on 23/03/2017.
//  Copyright © 2017 Rubio. All rights reserved.
//

import Foundation
import MapKit

extension MapsViewController {
  // MARK: Current Location
  func startLocationManager() {
    locationManager.requestAlwaysAuthorization()
    locationManager.requestWhenInUseAuthorization()
    
    if CLLocationManager.locationServicesEnabled() {
      locationManager.delegate = self
      locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
      locationManager.startUpdatingLocation()
    }
  }
  
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    centerMapOnLocation(location: manager.location!)
    locationManager.delegate = nil
  }
  
  // MARK: Map events
  func annotationFor(event: Event) -> MKAnnotation {
    let annotations = self.mapView.annotations.filter( { annotation in
      return annotation.title??.description == event.title
    })
    
    return annotations.first!
  }
  
  func centerMapOnLocation(location: CLLocation) {
    // Get coordinate region from location and center the map on it
    let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius * 2.0, regionRadius * 2.0)
    mapView.setRegion(coordinateRegion, animated: true)
  }
  
  func setCurrentLocation() {
    let latitude = UserDefaults.standard.float(forKey: kCurrentLocationLatitude)
    let longitude = UserDefaults.standard.float(forKey: kCurrentLocationLongitude)
    
    currentLocation = CLLocation(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude))
    centerMapOnLocation(location: currentLocation)
  }
  
  func didUpdateDistance() {
    setCurrentLocation()
  }
  
  func mapView(_ mapView: MKMapView, viewFor annotation:MKAnnotation) -> MKAnnotationView? {
    // Set buttons and title for each annotation on mapView
    if annotation is Event {
      var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "identifier")
      
      if annotationView == nil {
        annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "identifier")
        annotationView!.canShowCallout = true
        
        let btn = UIButton(type: .detailDisclosure)
        annotationView!.rightCalloutAccessoryView = btn
      }
      else {
        annotationView!.annotation = annotation
      }
      
      return annotationView
    }
    
    return nil
  }
  
  func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
    var i = -1;
    for view in views {
      i += 1;
      if view.annotation is MKUserLocation {
        continue;
      }
      
      // Check if current annotation is inside visible map rect, else go to next one
      let point:MKMapPoint  =  MKMapPointForCoordinate(view.annotation!.coordinate);
      if (!MKMapRectContainsPoint(self.mapView.visibleMapRect, point)) {
        continue;
      }
      
      let endFrame:CGRect = view.frame;
      
      // Move annotation out of view
      view.frame = CGRect(origin: CGPoint(x: view.frame.origin.x,y :view.frame.origin.y-self.view.frame.size.height), size: CGSize(width: view.frame.size.width, height: view.frame.size.height))
      
      // Animate drop
      let delay = 0.03 * Double(i)
      UIView.animate(withDuration: 0.5, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations:{() in
        view.frame = endFrame
        // Animate squash
      }, completion:{(Bool) in
        UIView.animate(withDuration: 0.05, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:{() in
          view.transform = CGAffineTransform(scaleX: 1.0, y: 0.6)
          
        }, completion: {(Bool) in
          UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:{() in
            view.transform = CGAffineTransform.identity
          }, completion: nil)
        })
        
      })
    }
  }
  
  func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
    // Set Current Event to the one selected
    let event = view.annotation as! Event
    currentEvent = event
    
    // Set Detail View
    titleDetailView.text = event.title
    imageDetailView.image = event.mainImage
    
    // Open Detail View
    self.animateIn(viewToAnimate: detailView, visualEffect: effect, visualEffectView: visualEffectView)
    
    // Not allow user make any movement
    filterButton.isEnabled = false
    refreshButton.isEnabled = false
  }
}
