//
//  PurchasesController.swift
//  Wiur
//
//  Created by Oxthor on 30/03/2017.
//  Copyright © 2017 Rubio. All rights reserved.
//

import Foundation
import UIKit
import StoreKit

class PurchasesController : UIViewController
{
  @IBOutlet weak var productName: UILabel!
  @IBOutlet weak var purchasedLabel: UILabel!
  @IBOutlet weak var priceLabel: UILabel!
  @IBOutlet weak var buyButton: UIButton!
  @IBOutlet weak var restoreButton: UIButton!
  
  var product: SKProduct?
  
  override func viewDidLoad() {
    WiurProducts.store.requestProducts{success, products in
      if success {
        self.product = products?.first
        self.productName.text = self.product?.localizedTitle
        self.priceLabel.text = self.product?.price.decimalValue.description
        print(self.product?.productIdentifier ?? "Unknown".localized())
        self.purchasedLabel.text = WiurProducts.store.isProductPurchased((self.product?.productIdentifier)!) ? "Purchased".localized() : "Not Purchased".localized()
      }
    }
  }
  
  @IBAction func buyProduct(_ sender: Any) {
    guard let product = product else {
      return
    }
    
    if !WiurProducts.store.isProductPurchased(product.productIdentifier) {
      WiurProducts.store.buyProduct(product)
    } else {
      presentDefaultAlert(title: "Error", message: "Product already purchased".localized())
    }
  }
  
  @IBAction func restoreProduct(_ sender: Any) {
    guard let product = product else {
      return
    }
    
    if WiurProducts.store.isProductPurchased(product.productIdentifier) {
      WiurProducts.store.restorePurchases()
    } else {
      presentDefaultAlert(title: "Error", message: "Product not purchased".localized())
    }
  }
}
