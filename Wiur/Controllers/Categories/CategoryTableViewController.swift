//
//  CategoryTableViewController.swift
//  Wiur
//
//  Created by Oxthor on 22/02/2017.
//  Copyright © 2017 Rubio. All rights reserved.
//

import Foundation
import UIKit

protocol CategoryTableViewDelegate: class {
  func didSelectCategory(category: String)
}

class CategoryTableViewController: UITableViewController
{
  var selectedCategory: String = ""
  var categories = ["Art".localized(),
                    "Culture".localized(),
                    "Music".localized(),
                    "Sports".localized(),
                    "Entertainment".localized(),
                    "Cinema".localized(),
                    "Party".localized(),
                    "Education".localized()]
  
  weak var delegate: CategoryTableViewDelegate?
  
  // MARK: Actions
  @IBAction func dismiss(_ sender: UIBarButtonItem) {
    self.dismiss(animated: true, completion: nil)
  }
  
  // MARK: TableView Data Source
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let cell = tableView.cellForRow(at: indexPath)
    let cellLabel = cell?.viewWithTag(1) as! UILabel
    
    if let cellText = cellLabel.text {
      selectedCategory = cellText
      delegate?.didSelectCategory(category: selectedCategory)
      self.dismiss(animated: true, completion: nil)
    }
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell")! as UITableViewCell
    let cellLabel = cell.viewWithTag(1) as! UILabel
    
    cellLabel.text = categories[indexPath.row]
    
    return cell
  }
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return categories.count
  }

}
