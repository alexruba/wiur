//
//  DataService.swift
//  Wiur
//
//  Created by Oxthor on 19/02/2017.
//  Copyright © 2017 Rubio. All rights reserved.
//

import Foundation
import Firebase

class DataService {
  static let dataService = DataService()
  
  private var _BASE_REF = Database.database().reference()
  private var _USER_REF = Database.database().reference().child("users")
  private var _EVENT_REF = Database.database().reference().child("events")
  private var _LIKES_REF = Database.database().reference().child("likes")
  
  var BASE_REF: DatabaseReference {
    return _BASE_REF
  }
  
  var USER_REF: DatabaseReference {
    return _USER_REF
  }
  
  var EVENT_REF: DatabaseReference {
    return _EVENT_REF
  }
  
  var LIKES_REF: DatabaseReference {
    return _LIKES_REF
  }
  
  private func createNewAccount(uid: String, user: Dictionary<String, String>) {
    USER_REF.child(uid).setValue(user)
  }
  
  func createNewEvent(uid: String, event: Dictionary<String, String>) {
    let firebaseNewEvent = EVENT_REF.childByAutoId()
    firebaseNewEvent.setValue(event)
  }
  
  func editEvent(uid: String, editedData: Dictionary<String, Any>) {
    EVENT_REF.child(uid).setValue(editedData)
  }
  
  func removeEvent(uid: String) {
    EVENT_REF.child(uid).removeValue()
  }
  
  //MARK: User
  func getUser(uid: String, with completion: @escaping (_ user: WUser)->()) {
    DataService.dataService.USER_REF.child(uid).observe(.value, with: { (snapshot) in
      let value = snapshot.value as? NSDictionary
      let username = value?["username"] as? String ?? "Unknown".localized()
      let email = value?["email"] as? String ?? "Unknown".localized()
      let name = value?["name"] as? String ?? "Unknown".localized()
      let uid = value?["uid"] as? String ?? "Unknown".localized()
      let avatarURL = value?["avatarURL"] as? String ?? "Unknown".localized()
      
      let user = WUser()
      user.username = username
      user.email = email
      user.name = name
      user.uid = uid
      user.avatarURL = avatarURL
      
      completion(user)
    })
  }
  
  func downloadAvatar(user: WUser, with completion: @escaping ()->()) {
    guard let avatarURL = user.avatarURL else {
      print("No user avatar set")
      return
    }
    
    Storage.storage().reference().child(avatarURL).getData(maxSize: 25*256*256, completion: { (data, error) in
      DispatchQueue.main.async {
        if error != nil {
          print(error!)
          return
        }
        let image = UIImage(data: data!)
        user.avatar = image
        
        completion()
      }
    })
  }
  
  // MARK: Images
  func downloadMainImage(for event: Event, with completion: @escaping ()->()) {
    Storage.storage().reference().child(event.mainImageURL).getData(maxSize: 25*256*256, completion: { (data, error) in
      if error != nil {
        print(error!)
        return
      }
      
      DispatchQueue.main.async {
        let image = UIImage(data: data!)
        event.mainImage = image!
        
        completion()
      }
    })
  }
  
  func downloadExtraImage(for event: Event, imageURL: String, with completion: @escaping (_ image: UIImage)->()) {
    Storage.storage().reference().child(imageURL).getData(maxSize: 25*256*256, completion: { (data, error) in
      DispatchQueue.main.async {
        event.extraImages.append(UIImage(data: data!)!)
        completion(UIImage(data: data!)!)
      }
    })
  }
  
  // MARK: Likes
  func like(event: Event, with user: WUser, completion: (()->())?) {
    Database.database().reference().child("events").child(event.uid).runTransactionBlock({ (currentData: MutableData) -> TransactionResult in
      if var post = currentData.value as? [String : AnyObject] {
        var likes : Dictionary<String, Bool>
        likes = post["likes"] as? [String :Bool] ?? [:]
        var likeCount = post["likeCount"] as? Int ?? 0
        if let _ = likes[user.uid] {
          likeCount -= 1
          likes.removeValue(forKey: user.uid)
          Database.database().reference().child("likes").child(user.uid).child(event.uid).removeValue()
        } else {
          likeCount += 1
          likes[user.uid] = true
          Database.database().reference().child("likes").child((user.uid)).child(event.uid).setValue(event.toDictionary())
        }
        post["likeCount"] = likeCount as AnyObject?
        post["likes"] = likes as AnyObject?
        
        currentData.value = post
        completion?()
        return TransactionResult.success(withValue: currentData)
      }
      completion?()
      return TransactionResult.success(withValue: currentData)
    }) { (error, committed, snapshot) in
      if let error = error {
        print(error.localizedDescription)
      }
    }
  }
  
  // MARK: Authentication
  func initLoggedUser(uid: String) {
    let user = WUser.sharedInstance
    
    USER_REF.child(uid).observe(.value, with: { (snapshot) in
      let value = snapshot.value as? NSDictionary
      user.updateWithData(data: value as! Dictionary<String, Any>)
    })
  }
  
  func registerUser(newUser: WUser, password: String, with completion: @escaping ()->(), failure: @escaping (_ errmsg: String)->()) {
    Auth.auth().createUser(withEmail: newUser.email, password: password) { (user, error) in
      
      if error == nil {
        let changeRequest = user?.createProfileChangeRequest()
        changeRequest?.displayName = newUser.username
        changeRequest?.commitChanges { error in
          if let error = error {
            print(error)
          } else {
            print("Profile updated")
            DataService.dataService.createNewAccount(uid: (user?.uid)!, user:
              ["uid": (user?.uid)!,
               "username": newUser.username,
               "email": newUser.email,
               "name": newUser.name,
               "type": newUser.type.rawValue.description])
            
            DataService.dataService.initLoggedUser(uid: (Auth.auth().currentUser?.uid)!)
            completion()
          }
        }
      } else {
        if let errorMsg = error?.localizedDescription {
          failure(errorMsg)
        } else {
          failure(NSLocalizedString("Errors ocurred", comment: ""))
        }
      }
    }
  }
  
  func signIn(email: String, password: String, with completion: @escaping ()->(), failure: @escaping (_ errmsg: String)->()) {
    Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
      
      if error == nil {
        DataService.dataService.initLoggedUser(uid: (Auth.auth().currentUser?.uid)!)
        completion()
      } else {
        if let errorMsg = error?.localizedDescription {
          failure(errorMsg)
        } else {
          failure("Something went wrong".localized())
        }
      }
    }
  }
}
