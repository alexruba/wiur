//
//  EditEventViewController.swift
//  Wiur
//
//  Created by Oxthor on 03/03/2017.
//  Copyright © 2017 Rubio. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import DatePickerDialog
import MapKit

class EditEventViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, EventFormDelegate
{
  
  @IBOutlet var extraImageButtons: [UIButton]!
  
  var currentImageButton = UIButton()
  var event: Event = Event()
  let defaultExtraImage = #imageLiteral(resourceName: "image-template")
  
  var imagesProcessed = 0
  var imagesCount = 0
  
  // MARK: Publish Event
  
  @IBAction func publishEvent(_ sender: UIBarButtonItem) {
    let errors = event.isCompleted()
    if !errors.isEmpty {
      self.presentDefaultAlert(title: "Error", message: errors)
      return;
    }
    
    // Count extra images to check the upload task
    imagesCount = getExtraImagesCount()
    if imagesCount <= 0 { addNewEvent(); return }
    // Upload extra images
    for extraImageButton in self.extraImageButtons {
      if isExtraImageSet(extraButton: extraImageButton) {
        let extraImageUploadTask = event.uploadExtraImage(image: extraImageButton.currentImage!)
        
        extraImageUploadTask.observe(.success) { snapshot in
          self.imageProcessed()
        }
      }
    }
  }
  
  func imageProcessed() {
    imagesProcessed += 1
    if imagesProcessed >= imagesCount {
      addNewEvent()
    }
  }
  
  func addNewEvent() {
    uploadEventData()
    showSuccessMessage()
  }
  
  func uploadEventData() {
    let eventDictionary = event.toDictionary()
    DataService.dataService.EVENT_REF.child(event.uid).updateChildValues(eventDictionary)
  }
  
  func showSuccessMessage()
  {
    let alertController = UIAlertController(title: NSLocalizedString("Evento modificado", comment: ""), message: "Se han realizado las modificaciones pertinentes en el evento.".localized(), preferredStyle: .alert)
    
    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: { _ in
      self.dismiss(animated: true, completion: nil)
    })
    
    alertController.addAction(defaultAction)
    self.present(alertController, animated: true, completion: nil)
  }
  
  // MARK: Event photo
  @IBAction func didTapImage(_ sender: UIButton) {
    let picker = UIImagePickerController()
    picker.delegate = self
    currentImageButton = sender
    
    present(picker, animated: true, completion: nil)
  }
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    var selectedImageFromPicker: UIImage?
    
    if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
      selectedImageFromPicker = originalImage
    }
    
    if let selectedImage = selectedImageFromPicker {
      self.currentImageButton.setImage(selectedImage, for: .normal)
    }
    
    picker.dismiss(animated: true, completion: nil)
  }
  
  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    picker.dismiss(animated: true, completion: nil)
  }
  
  // MARK: Upload images utilities
  func isExtraImageSet(extraButton: UIButton) -> Bool {
    return extraButton.currentImage! != defaultExtraImage
  }
  
  func getExtraImagesCount() -> Int {
    var count = 0
    
    for extraImagesButton in extraImageButtons {
      if isExtraImageSet(extraButton: extraImagesButton) {
        count += 1
      }
    }
    
    return count
  }
  
  // MARK: Event Form delegate
  func didFillName(name: String) {
    self.event.title = name
  }
  
  func didFillPrice(price: Double) {
    self.event.price = price
  }
  
  func didFillStartDate(startDate: Date) {
    self.event.start = startDate
  }
  
  func didFillEndDate(endDate: Date) {
    self.event.end = endDate
  }
  
  func didFillWebpage(webpage: String) {
    self.event.website = webpage
  }
  
  func didFillCategory(category: String) {
    self.event.category = category
  }
  
  func didFillTicketsURL(ticketsURL: String) {
    self.event.ticketsURL = ticketsURL
  }
  
  func didFillLocation(location: CLPlacemark) {
    self.event.address = (location.addressDictionary?["Thoroughfare"] as! String?)!
    self.event.cp = location.addressDictionary?["ZIP"] as? String
    self.event.town = location.addressDictionary?["City"] as? String
    self.event.coordinate = (location.location?.coordinate)!
  }
  
  func didFillDescription(eventDescription: String) {
    self.event.info = eventDescription
  }
  
  // MARK: Prepare for segue
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "embed-event-form" {
      let vc = segue.destination as! EventFormViewController
      vc.delegate = self
      vc.event = event
    }
  }
}
